## example\_data\FCSFitM
Contains example correlation data for fluorescent dye abd fluorescent protein. Before performing a fit of the correlation for the fluorescent protein FCSFitM requests to compute the fluorescene intensities at the FCS measurment point. For this use FCSImageBrowser.

* *Alexa*: Data obtained from a fluroescent dye, preprocessed with Fluctuation Analyzer (FA)
* *Alexa_nofa*: Data obtained from a fluroescent dye without preprocessing with FA
* *APDsFormat*: Readin data from a fluroescent dye acquired with APD or GaAsP detectors
* *Protein*: Example data for a fluroscent protein.


## example\_data\FCSCalibration
Example data for FCSCalibration R app

* *nobatch* contains data for only one protein. GUI `Batch` unchecked.
* *batch* contains data for two proteins. GUI `Batch` checked.




![batch_on_off.PNG](.\resources\batch_on_off.PNG)
