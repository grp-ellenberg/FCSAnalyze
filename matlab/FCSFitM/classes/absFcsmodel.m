%%
% absFcsmodel abstract class for fitting FCS data to using the autocorrelation curve that comes from
% Fluctualize M. Wachsmuth
%
% EMBL, Antonio Politi, November 2016
%
classdef absFcsmodel < handle
    properties (Access = public, Abstract)
        % these are the default parameters for a protein
        %par =   struct('N', 10,     'thetaT', 0.2,  'tauT', 100, 'f1', 1, 'tauD1', 500, 'alpha1', 1,
        %'tauD2', 5000,  'alpha2', 1,    'kappa', 5.5);  % parameter of model lb =    struct('N', 0.0001,
        %'thetaT', 0.001,'tauT', 0.1, 'f1', 1, 'tauD1', 100, 'alpha1', 0.5,  'tauD2', 500,   'alpha2', 0.5,
        %'kappa', 1); hb =    struct('N', 10000,  'thetaT', 1, 'tauT', 1000,  'f1', 1,  'tauD1',
        %50000,'alpha1', 1.2, 'tauD2', 500000,  'alpha2', 2,  'kappa', 20);
        
        par; % parameter values a struct with name of parameter and value. This needs to be defined in the derived class
        lb;  % low boundary of parameter values
        hb;  % high boundary of parameter values
        tauBoundary;
        model;
        
    end
    
    properties (Access = public)
        weightChi2 = 1; % 1, use weighted Chi2; 0 use unweighted Chi2 
        maxfittry  = 5; % number of repetitions for fit
    end
    
    methods (Abstract)
        Gcomp(obj); %model to compute the ACF given the parameters
    end
    methods
      
        function MO = absFcsmodel(parnames, parvalues, lbvalues, hbvalues)
            % ABSFCSMODEL  constructor
            %   MO = absFcsmodel() - just create class MO = absFcsmodel(parnames, parvalues) - create class and
            %   assign parvalues to parameters
            %       named in parnames.
            %   MO = absFcsmodel(parnames, parvalues) - create class and assign parvalues and lower boundary to
            %   parameters
            %       named in parnames.
            %   MO = absFcsmodel(parnames, parvalues)- create class and assign parvalues, lower and higher
            %   boundary to parameters
            %       named in parnames.
            %   parnames:  cell array containing name of paramers parvalues: vector with values of parameters
            %   lbvalues:  vector with values of lower bounds hbvalues:  vector with values of higher bounds
            
            if nargin < 3 % should this not be 2? TODO
                return
            end
            % update default parameter
            for iname = 1:size(parnames, 1)
                name = parnames{iname};
                assert(isfield(MO.par, name), [name  ' Parameter does not exist!'])
                setfield(MO.par,name, parvalues(iname));
                if nargin < 4
                    return
                end
                setfield(MO.lb, name, lbvalues(iname));
                if nargin < 5
                    return
                end
                setfield(MO.hb, name, hbvalues(iname));
            end
        end
        
        function [idxs] = getDataIndexes(MO, data, tauBoundary)
            % GETDATAINDEXES get indexes within tauBoundary
            %   data        - is [tau, corr] array 
            %   tauBoundary - is [], [tauStart], [tauStart, tauEnd] 
            %   idxs        - are indexes of where to start (base 1) and
            %   where to end. idxs = [1, 0] use whole data set. Compatible
            %   with FA notation
            tauStart = data(1,1);
            tauEnd = data(end,1);
            if nargin == 3 
                if ~isempty(tauBoundary)
                    tauStart = tauBoundary(1);
                    if length(tauBoundary) == 2
                        tauEnd = tauBoundary(2);
                    end
                end
            end
            ist= find(data(:,1) >= tauStart); % index of start time
            ien = find(data(:,1) >= tauEnd);  % index of end time
            idxs(1) = ist(1);
            idxs(2) = size(data,1) - ien(1);
        end
        
        function [N, tauD1, tauD2] = initialGuess(MO, data, tauBoundary)
            % INITIALGUESS  given the AC data returns an initial guess forN, tauD1 of first component, tauD2 of
            % second component
            %   [N, tauD1, tauD2] = MO.initialGuess(data, tauStart, tauEnd)
            %       data     - is [tau, corr] array 
            %       tauStart - is value in us of time where to start the analysis
            %       tauEnd   - is value in us of time where to end analysis
            %   Method from Wachsmuth, M., et al.  (2015). Nature Biotechnology, 33(4), 384?389.
            %   https://doi.org/10.1038/nbt.3146
            
            if nargin < 3
                tauBoundary = MO.tauBoundary;
            end
            idxboundary = MO.getDataIndexes(data, tauBoundary);
            data = data(idxboundary(1):end-idxboundary(2),:);
            % only positive values for N and tauDm
            idx = find(data(:,2)>0);
            data = data(idx, :);
            N = 1/mean(data(1:5,2)); 
            % compute integral of equation to obtain a gues of average time
            % only positive values
            
            tauDm = N*trapz(data(:,1), data(:,2))/4.5; 
            %randomize 
            N = N + (1-2*rand)*N/8;  
            % another way of computing tauDm would be by using a linear approximation of AC
            tauDm = tauDm + (1-2*rand)*tauDm/8;                    % randomize tauDm
            % assign value of tauD1 and tauD2 so that tauD1 ~ 1/10 of tauD2. 1/tauDm = f1/tauD1 + (1-f1)/tauD2
            tauD1 = (9*MO.par.f1+1)/10*tauDm;
            tauD2 = (9*MO.par.f1+1)*tauDm;
        end
        
        function Gdiff = Gdiffcomp(MO, par, data, weight)
            % GDIFFCOMP computes weighted difference of ACF data and model
            %   par  - full parameter struct of model data - Ntx3 matrix of data [time, ACF, ACF_std]
            if nargin < 4
                weight = MO.weightChi2;
            end
            if  weight
                Gdiff = (MO.Gcomp(par, data(:,1)) - data(:,2))./data(:,3);
            else
                Gdiff = (MO.Gcomp(par, data(:,1)) - data(:,2));
            end
        end
        
        
        
        function G = Gcompfit(MO, parf, names, par, tau)
            % GCOMPFIT computes model ACF using parameter specified for fit
            %   parf  - parameter vector names - name of parameter to be changed par   - default parameter
            %   struct tau   - vector of ACF times
            for i = 1:length(names)
                par = setfield(par, names{i}, parf(i));
            end
            G = MO.Gcomp(par, tau);
        end
        
        function Gdiff = Gdiffcompfit(MO, parf, names, par, data, weight)
            % GDIFFCOMPFIT computes difference model to ACF using parameter specified for fit
            %   parf  - parameter vector names - name of parameter to be changed par   - default parameter
            %   struct tau   - vector of ACF times
            for i = 1:length(names)
                par = setfield(par, names{i}, parf(i));
            end
            if nargin < 6
                weight = MO.weightChi2;
            end
            Gdiff = MO.Gdiffcomp(par, data, weight);
                
        end
        
        function [par, norm] = getbestfit(MO, parCA,normV)
            % GETBESTFIT returns parameter and norm of best fit from
            % several fits
            %   parCA - a cell array
            %   normV - a vector containing the norm
            [val, pos] = min(normV);
            norm = normV(pos);
            par = parCA{pos};
        end
        
        function [R2, SSres] = compR2(MO, data, par, tauBoundary, weight)
            %%
            % COMPR2 computes R2 given the parameters 
            % Note that we are fitting a non-linear model so this has not
            % much meaning for comparing models! It is just used to asses
            % curves that are kind of flat.
            % R2adj is used in FA 
            if nargin < 4
                tauBoundary = MO.tauBoundary;
            end
            if nargin < 5
                weight = MO.weightChi2;
            end
            idxboundary = MO.getDataIndexes(data, tauBoundary);
            data = data(idxboundary(1):end-idxboundary(2),:);
            if isstruct(par)
                SSres = sum(MO.Gdiffcomp(par, data, weight).^2);
            else
                for i=1:size(par,1)
                    parS = MO.parvecToparstruct(par(i,:));
                    SSres(i,1) = sum(MO.Gdiffcomp(parS, data, weight).^2);
                end
            end
            if weight
                SStot = sum((data(:,2)./data(:,3) - 1./data(:,3)*mean(data(:,2))).^2);
                
            else
                SStot = sum((data(:,2) - mean(data(:,2))).^2);
            end
            
            R2 = 1- SSres./SStot;
            
            % R2adj is corrected for the number of parameters that have
            % been fitted 
            % R2adj_2c = 1 - (1-R2_2c)*(Nrpts2c-1)/(Nrpts2c - nrpara2c - 1) (https://en.wikipedia.org/wiki/Coefficient_of_determination#Adjusted_R2)
            % We don't pass the Npar fitted to the function here
        end
        
        function parS = parvecToparstruct(MO, parV)
            % PARVECTOPARSTRUCT return a struct of type MO.par with values given by parV
            %   MO.parvecToparstruct(parV) parV  - parameter vector of length MO.par
            parS = MO.par;
            parnames = fieldnames(MO.par);
            for i=1:length(parnames)
                parS = setfield(parS, parnames{i}, parV(i));
            end
        end
        
        function plotTrace(MO, par, data)
            % PLOTTRACE plot model and exp data in a graph
            %   par  - full parameter struct data - Ntx3 matrix of data [time, ACF, ACF_std]
            G = MO.Gcomp(par, data(:,1));
            semilogx(data(:,1), G,'-', data(:,1), data(:,2),'o');
        end
        
        function [par, norm] = fitmodel(MO, parf, names, par, data, tauBoundary)
            % FITMODEL fit model to data depending weightChi2 this use weighted difference or not
            %   [par, norm] = MO.fitmodel(parf, names, par, data) - fit in default time interval (set by the
            %   data) [
            %   [par, norm] = MO.fitmodel(parf, names, par, data, tauBoundary) - fit in time intervael
            %   [tauBoundary(1), tauMax] or [tauBoundary(1) tauBoundary(2)]
            %   if tauBoundary has length 2
            %  INPUT:
            %       parf  - vector of parameter values for the fit names - cell array with names of parameters.
            %       par   - struct of parameters data  - Ntx3 matrix of data [time, ACF, ACF_std] tauStart -
            %       start time in us for fitting domain tauEnd   - end time in us for fitting domain
            %   OUTPUT:
            %       par  - struct containing fitted parameters norm - sum of squared difference at fitting
            %       minima
            %       norm - sum of squared residuals at minima
            
            if nargin < 6
                tauBoundary = MO.tauBoundary;
            end
            idxboundary = MO.getDataIndexes(data, tauBoundary);
            data = data(idxboundary(1):end-idxboundary(2),:);
            lb = [];
            hb = [];
            for i = 1:length(names)
                lb = [lb;getfield(MO.lb,names{i})];
                hb = [hb;getfield(MO.hb,names{i})];
            end
            options = optimset('display','off');
            [parf, norm] = lsqnonlin(@MO.Gdiffcompfit, parf, lb, hb, options, names, par, data);
            for i = 1:length(names)
                par = setfield(par, names{i}, parf(i));
            end
        end
        
        
        function [AC_all, mCountRate_all] = readZeissfcs(MO, fnames)
            % READZEISSFCS reads *.fcs generated in Zeiss ZEN and returns a
            % cell arrays containing correlations and mCountrates. Function just calls the readZeissfcs_sf for a single file 
            %   MO.readZeissfcs(fnames) - read data containe in several files. fnames: cell array of filenames 
            %   OUTPUT (see readZeissfcs_sf):
            %       AC_all       -  Cell array containing the AC for each file, and repetition: {time, AC1_1, AC1_1_std, AC2_2, AC2_1_std, CC_1, CC_1_std}, {time, AC1_2, ... }.
            %                    Time is converted to us. AC has been converted to have base 0.
            %                    AC1_1_std = 1 as ZEISSFCS has no std information.
            %       mCountRate_all - array containing mean Count rate in kHz stored as [Counts_Ch1_file1, Counts_Ch2_file1, Counts_CC_file1; Counts_Ch1_file2, Counts_Ch2_file2, Counts_CC_file2]. 
            %                    Countrate_CC is always 0. Ch1 and Ch2 are set 
            %                    with FA convention. 
            % For APD's Channels are swapped. 
            
            if nargin < 2
                fnames = {fullfile(fileparts(mfilename('fullpath')), '.\example_data\Alexa\488_561nm.fcs')};
            end
            if ~iscell(fnames)
                fnames = {fnames};
            end
            AC_all = {};
            mCountRate_all = [];
            for i = 1:length(fnames)
               [AC, mCountRate] = MO.readZeissfcs_sf(fnames{i});
               AC_all = {AC_all{:}, AC{:}};
               mCountRate_all = [mCountRate_all; cell2mat(mCountRate')];
            end
        end
        
        function [AC, mCountRate] = readZeissfcs_sf(MO, fname)
            % READZEISSFCS_SF read AC data that has been stored in a Zeiss ZEN software *.fcs file and returns a
            % cell arrays for autocorrelations and mean count rates
            %   MO.readZeissfcs(fname) - read data in fname where fname is path to the .fcs file
            %   OUTPUT:
            %       AC        -  Cell array containing the AC for each measurement: {time, AC1_1, AC1_1_std, AC2_2, AC2_1_std, CC_1, CC_1_std}, {time, AC1_2, ... }.
            %                    Time is converted to us. AC has been converted to have base 0.
            %                    AC1_1_std = 1 as ZEISSFCS has no std information.
            %       mCountRate - mean Count rate in kHz stored as [Countrate_Ch1, Countrate_Ch2, Countrate_CC]. Countrate_CC is always 0. Ch1 and Ch2 are set 
            %                    with FA convention. 
            % For APD's Channels are swapped. 
            
            if nargin < 2
                fname = fullfile(fileparts(mfilename('fullpath')), '.\example_data\Alexa\488_561nm.fcs');
            end
            fileID = fopen(fname);
            % read whole file at once
            C = textscan(fileID, '%s', 'delimiter','\n');
            fclose(fileID);
            %%
            % find repetion position and channel
            RPC = []; % [Repetition_index Point_index Channel_index]
            positionIdx = find(strncmp(C{1}, 'Position = ', 11));
            repetitionIdx = find(strncmp(C{1}, 'Repetition = ', 13));
            channelIdx = find(strncmp(C{1}, 'Channel = ', 10));
            for i = 1:length(positionIdx)
                tmp = C{1}(positionIdx(i));
                [tokp] = regexp(tmp{:},'(-?\d+)',  'tokens');
                tmp = C{1}(repetitionIdx(i));
                [tokr] = regexp(tmp{:},'(-?\d+)',  'tokens');
                tmp = C{1}(channelIdx(i));
                [tokc] = regexp(tmp{:},'(-?\d+)',  'tokens');
                ip = str2double(char(tokp{:}));
                ir = str2double(char(tokr{:}));
                if isempty(tokc)
                    continue
                end
                ich_in = str2double(char(tokc{1}));
                if (ip < 0 || ir < 0 || ich_in < 0)
                    continue
                end
                
                % check if channel is cross-correlation
                isac = isempty(findstr('Cross-correlation', tmp{:}));
                % check if channel is apd or 
                isapd = isempty(findstr('Meta', tmp{:}));

                % invert channel for compability with FA for APD
                % keep same channels for ChS1 and ChS2
                if ~isac 
                    ich = 3;
                else
                  if isapd  
                    if ich_in == 2
                        ich = 1;
                    elseif ich_in == 1
                        ich = 2;
                    end
                  else
                      ich = ich_in;
                  end
                end
                RPC = [RPC; ir+1 ip+1 ich ];
            end

            
            % index for measurements is based on repetition or measured points
            idxR = 2;
            if max(RPC(:,1))>1
                idxR = 1;
            end
            if max(RPC(:,2))>1
                idxR = 2;
            end

          
            %%
            %Index where correlation data starts
            AC_idx = find(strncmp(C{1}, 'CorrelationArray =',18 )); %this reads all entries.
            % contains auto and eventually cross-correlation data for each repetition/position
            AC = cell(1,max(max(RPC(:,1:2))));
            %In fcs file last index contains only fitting information.
            %Therefore only process from 1:length(AC_idx)-1
            for i=1:length(AC_idx)
                % get dimension
                dim = str2num(C{1}{AC_idx(i)}(19:end));
                if dim(1) == 0
                    continue
                end
                %AC for current repetition/position
                ACl = cell2mat(cellfun(@str2num, C{1}(AC_idx(i)+1:AC_idx(i)+dim(1)), 'un', 0));
                idx = RPC(i, idxR);
                if isempty(AC{idx})
                    %[time, AC1_1, AC1_1_std, AC2_2, AC2_1_std, CC_1, CC_1_std]
                    % std are set to 1 as these are not computed in the
                    % Zeiss module but only using fluctuationAnalyzer
                    AC{idx} = zeros(size(ACl,1), 7);
                end
                % time is converted in us, AC base is set to 0
                AC{idx}(:,1) = ACl(:,1)*1e6;
                AC{idx}(:,RPC(i,3)*2:RPC(i,3)*2+1) = [ACl(:,2)-1 ones(size(ACl,1),1)];
            end
            
            % indexes where where CountRateArray start
            CountRate_idx = find(strncmp(C{1}, 'CountRateArray =',16)); %this reads all entries for CountRate
            % contains only mean of countrate rest is not needed 
            mCountRate = cell(1,max(max(RPC(:,1:2))));
            %last entry is fitting etc.
            for i=1:length(CountRate_idx)
                dim = str2num(C{1}{CountRate_idx(i)}(17:end));
                if dim(1) > 0
                    CountRatel = cell2mat(cellfun(@str2num, C{1}(CountRate_idx(i)+1:CountRate_idx(i)+dim(1)), 'un', 0));
                    mCountRatel = mean(CountRatel(:,2));
                    
                    idx = RPC(i, idxR);
                    if isempty(mCountRate{idx})
                        mCountRate{idx} = zeros(1,3);
                    end
                    mCountRate{idx}(:,RPC(i,3)) = mCountRatel/1000;
                end
            end
        end
        
        function AC = readFAcor(MO, fname)
            % READFACOR read AC data that has been generated with Fluctualizer
            %   MO.readFAcor(fname) -
            %       fname: name of .cor file. If fname is a cell array than all files are read at once
            %   output:
            %       AC: cell array containing the AC  as - time AC_Ch1 std_ACh1 ACh2 std_ACh2 XC_Ch1Ch2 std_XC_Ch1Ch2
            if nargin < 1
                fname = {'W:\Shotaro\Live-cell-imaging\HeLa-FCS\160701-HeLa-Nup107\160701_gfpNUP107z26z31\Calibration\Alexa\488nm_R1_P1_K1_Ch2.zen.kappa55.cor'};
            end
            if ~iscell(fname)
                fname = {fname};
            end
            for i=1:length(fname)
                AC{i} = dlmread(fname{i}, '\t', 1, 0);
                % add columns as required to have all 6 entries Ch1, Ch2, XC
                if size(AC{i},2) < 7
                    AC{i}(:,size(AC{i},2):7) = 0;
                end
            end
            
        end
        
        function AC = readFAxml(MO, fnames)
            % READFAXML read AC, parameters, annotations etc from xml file generated per measurement by FA
            if nargin < 2
                %example file
                path = fileparts(fileparts(mfilename('fullpath')));
                
                fnames = {fullfile(path, 'example_data', '488_561nm_R1_P1_K1_Ch2-Ch1.zen.1c.xml')}
            end
            for i=1:length(fnames)
                % each fnames contains one measurement only
                AC{i} = xmlFA.readcor(fnames{i});
            end
        end
        
                function [outPar, norm, R2, norms, R2s,  Nrpt, nrpara] = fitOneTwoComponents(MO, data, XC, twoC)
            % FITONETWOCOMPONENTS fit model with one or 2 components 
            %   twoC == 1: fit N, tauD1, alpha1, tauD2, alpha2, f1, (thetaT) 
            %   twoC == 0: fit N, tauD1, alpha1,  (thetaT)
            %   tauT is kept constant at default value
            %   INPUT:
            %       data   - correlation data [tau, corr]
            %       XC     - 0 or 1 whether data is cross-correlation. In
            %       this case thetaT = 0
            %       twoC   - 0 or 1 depending whether we fit a one
            %       component model
            %   OUTPUT:
            %       outPar - all parameter values of the model as vector
            %       norm   - norm at optimum
            %       R2     - R2 at optimum
            %       norms  - unweighted and weighted norm
            %       R2s    - unweighted and weighted R2
            %       Nrpt   - Nrpoints fitted
            %       nrpara - number of parameters fitted
            %   Workflow is firts to fit N, tauD1, tauD2, f1; then N,tauD1,
            %   alpha1,  tauD2, alpha2, f1; finally  N, tauD1, alpha1,  tauD2,
            %   alpha2, f1, thetaT
            
            if nargin < 3
                XC = 0;
            end
            
            idxboundary = MO.getDataIndexes(data, MO.tauBoundary);
            data = data(idxboundary(1):end-idxboundary(2),:);
            
            %% initial parameters
            pari = MO.par;
            if XC
                pari.thetaT = 0;
            end
            pari.f1 = 0.5*(twoC==1) + 1*(twoC==0);
            
            %% fit workflow
            % find initial value for N, tauD1, (tauD2, f1) and repeat
            for itry = 1:MO.maxfittry
                [N, tauD1, tauD2] = MO.initialGuess(data, MO.tauBoundary); % this includes some random variations of parameters
                if twoC
                    [parf{itry}, normT(itry)] = MO.fitmodel([N, tauD1, tauD2, pari.f1+(1-2*rand)*pari.f1/8], {'N', 'tauD1', 'tauD2', 'f1'}, pari, data);
                else
                    [parf{itry}, normT(itry)] = MO.fitmodel([N, tauD1], {'N', 'tauD1'}, pari, data, MO.tauBoundary);
                end
            end
            parf1 = MO.getbestfit(parf, normT);
            
            % fit N, tauD1, alpha1, (tauD2, alpha2, f1)
            for itry = 1:MO.maxfittry
                if twoC
                    [parf{itry}, normT(itry)] = MO.fitmodel([parf1.N, parf1.tauD1, parf1.tauD2, ...
                        parf1.f1, pari.alpha1+(1-2*rand)*pari.alpha1/8, pari.alpha2+(1-2*rand)*pari.alpha2/8], {'N', 'tauD1', 'tauD2', 'f1', 'alpha1', 'alpha2'}, pari, data);
                else
                    [parf{itry}, normT(itry)] = MO.fitmodel([parf1.N, parf1.tauD1, pari.alpha1+(1-2*rand)*pari.alpha1/8], {'N', 'tauD1', 'alpha1'}, pari, data, MO.tauBoundary);
                end
                
            end
            parf1 = MO.getbestfit(parf, normT);
            
            % fit N, tauD1, alpha1, (tauD2, alpha2, f1) and thetaT
            if ~XC
                for itry = 1:MO.maxfittry
                    if twoC
                        [parf{itry}, normT(itry)] = MO.fitmodel([parf1.N, parf1.tauD1, parf1.tauD2, ...
                            parf1.f1, parf1.alpha1, parf1.alpha2, pari.thetaT+(1-2*rand)*pari.thetaT/8], {'N', 'tauD1', 'tauD2', 'f1', 'alpha1', 'alpha2', 'thetaT'}, pari, data);
                    else
                        [parf{itry}, normT(itry)] = MO.fitmodel([parf1.N, parf1.tauD1,  parf1.alpha1 , pari.thetaT+(1-2*rand)*pari.thetaT/8, ...
                            ], {'N', 'tauD1', 'alpha1', 'thetaT'}, parf1, data);
                    end
                end
            end
            [parf2, norm] = MO.getbestfit(parf, normT);
            
            %% format the output
            outPar(1,:) = struct2array(parf2);
            R2 = MO.compR2(data, outPar);
            % sum of squares unweighted and weighted
            norms(1) = sum(MO.Gdiffcomp(parf2, data, 0).^2);
            norms(2) = sum(MO.Gdiffcomp(parf2, data, 1).^2);
            % R2 unweighted and weighted
            R2s(1) = MO.compR2(data, parf2, [], 0);
            R2s(2) = MO.compR2(data, parf2, [], 1);
            Nrpt = length(data);
            
            if XC
                nrpara = 6*(twoC==1) + 3*(twoC==0);
            else
                nrpara = 7*(twoC==1) + 4*(twoC==0);
            end
        end
        
        function [outPar, norm, R2, norms, R2s,  Nrpt, nrpara] = fitTwoComponents(MO, data, XC)
            % FITTWOCOMPONENTS fit N, tauD1, alpha1, tauD2, alpha2, f1, thetaT
            %   INPUT:
            %       data   - correlation data [tau, corr]
            %       XC     - 0 or 1 whether data is cross-correlation. In
            %       this case thetaT = 0
            %   OUTPUT:
            %       outPar - all parameter values of the model as vector
            %       norm   - norm at optimum
            %       R2     - R2 at optimum
            %       norms  - unweighted and weighted norm
            %       R2s    - unweighted and weighted R2
            %       Nrpt   - Nrpoints fitted
            %       nrpara - number of parameters fitted
            %
            %   Workflow is firts to fit N, tauD1, tauD2, f1; then N,tauD1,
            %   alpha1,  tauD2, alpha2, f1; finally  N, tauD1, alpha1,  tauD2,
            %   alpha2, f1, thetaT
            
            if nargin < 3
                XC = 0;
            end
            [outPar, norm, R2, norms, R2s,  Nrpt, nrpara] = fitOneTwoComponents(MO, data, XC, 1);
        end
        
        function [outPar, norm, R2, norms, R2s, Nrpt, nrpara] = fitOneComponent(MO, data, XC)
            % FITONECOMPONENT fit N, tauD1, alpha1,  thetaT. tauT is kept
            % constant at default value
            %   INPUT:
            %       data   - correlation data [tau, corr]
            %       XC     - 0 or 1 whether data is cross-correlation. In
            %       this case thetaT = 0
            %   OUTPUT:
            %       outPar - all parameter values of the model as vector
            %       norm   - norm at optimum
            %       R2     - R2 at optimum
            %       norms  - unweighted and weighted norm
            %       R2s    - unweighted and weighted R2
            %       Nrpt   - Nrpoints fitted
            %       nrpara - number of parameters fitted
            %
            %   Workflow is firts to fit N, tauD1; then N,tauD1, alpha1;
            %   then N, tauD1, alpha1, thetaT. If cross 
            
            if nargin < 3
                XC = 0;
            end
            [outPar, norm, R2, norms, R2s,  Nrpt, nrpara] = fitOneTwoComponents(MO, data, XC, 0);
        end
 
    end
end

