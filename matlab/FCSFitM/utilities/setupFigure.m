function h = setupFigure(ifig, figsize, varargin)
%%
% h = setupFigure(ifig,figsize) creates a figure and set some standard properties
%   INPUT:
%       ifig: number of figure
%       figsize: vector [ posx posy width height]
%   OUTPUT:
%       h: handle of figure
% Additional options
% h = setupFigure(ifig,figsize, unit) 
% h = setupFigure(ifig,figsize, unit, fontsize)
% h = setupFigure(ifig,figsize, unit, fontsize, visible) 
%   OPTIONAL INPUT:
%       unit: 'pixel' or 'cm' unit of the figure
%       fontsize: integer for the fontsize of axis and tickmarks
%       visible:  'on' or 'off' set the visibility of the figure


if nargin > 2
    Visible = varargin{1};
else
    Visible = 'on';
end

if nargin > 3
    units = varargin{2};
else
    units = 'pixel';
end

if nargin >4
    fontSize = varargin{3};
else
    fontSize = 12;
end
if nargin > 5
    box = varargin{4};
else
    box = 'off';
end

try
    if ifig> 0
        h = figure(ifig);
        set(gcf, 'Visible', Visible);
    else
        h = figure('Visible', Visible);
        set(gcf, 'Visible', Visible);
    end
catch
    if ifig> 0
        h = figure(ifig);
        set(gcf, 'HandleVisibility', Visible);
    else
        h = figure('HandleVisibility', Visible);
        set(gcf, 'Visible', Visible);
    end
end

clf
hold('on');
%set(h,'OuterPosition',figsize);
set(gcf,'Position',figsize);
try
set(gcf,'ActivePositionProperty','Position')
end
set(gcf, 'units', units, 'pos', figsize)
%set(gcf, 'Color', [1 1 1]); % Sets figure background
%set(gca, 'Color', [1 1 1]); % Sets axes background
%set(gcf, 'Renderer', 'painters');
set(gcf, 'PaperPositionMode', 'auto');
set(gca,'Box',box,'FontSize',fontSize);
end
