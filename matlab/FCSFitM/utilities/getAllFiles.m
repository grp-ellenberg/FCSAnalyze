function fileList = getAllFiles(dirName, extension, file_pattern_matching, recursive)
% GETALLFILES Recursively finds all the files in a folder and its subfolder with 'extension'
% extension specified by the 3 digit string 'extension'
%     dirName - root directory name
%     extension - string for extension
%     file_pattern_matching - a string contained in filename
% Moshir Harsh, EMBL, November 2016
% Antonio Politi, EMBL, January 2017 added file_patter_matching
if nargin < 3
    file_pattern_matching = [];
end
if nargin < 4
    recursive = 1;
end
%TODO: Add a pattern matching clause to match file names
dirData   = dir(dirName);      %# Get the data for the current directory
dirIndex  = [dirData.isdir];   %# Find the index for directories
dirData2 = dir(fullfile(dirName, ['*.' extension])); %Find only the files with the specific extension

%dirData2 = dirData2(~cellfun('isempty', {dirData2.name})); %To remove the invalid output of dir if the folder is found empty of reqd files
fileList = {dirData2.name}';  %'# Get a list of the files
% perform pattern matching
if ~isempty(file_pattern_matching)
    idx = [];
    for i=1:length(fileList)
        if ~isempty(regexp(fileList{i}, file_pattern_matching, 'match'))
            idx = [idx;i];
        end
    end
    fileList = fileList(idx);
end
if ~isempty(fileList)
    fileList = cellfun(@(x) fullfile(dirName,x),...  %# Prepend path to files
        fileList,'UniformOutput',false);
end
subDirs    = {dirData(dirIndex).name};             %# Get a list of the subdirectories
validIndex = ~ismember(subDirs,{'.','..'});     %# Find index of subdirectories
%#   that are not '.' or '..'
if recursive
    for iDir = find(validIndex)                     %# Loop over valid subdirectories
        nextDir = fullfile(dirName,subDirs{iDir});    %# Get the subdirectory path
        fileList = [fileList; getAllFiles(nextDir, extension, file_pattern_matching, recursive)];  %# Recursively call getAllFiles
    end
end

end