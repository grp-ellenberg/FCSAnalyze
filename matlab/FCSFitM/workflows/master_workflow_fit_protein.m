function master_workflow_fit_protein(wd)
% MASTER_WORKFLOW_FIT_PROTEIN call workflow_fit_protein for several directorie
% specified in the script
% INPUT:
%   wd - working directory to search recursively for folder named '(\Mito(s|S)ys\d)|(LSM880)'
% OUTPUT:
%   the function does not give an output. It creates the file wd/ProcessingHelpFiles/protein2cfiles.mat
%   The mat file contains a list of the directories to process.
% When using the GUI this function is not required. 
% Antonio Politi, EMBL, January 2017

session = '2c'
force = 0;
if nargin <1
    wd = uigetdir('.', 'Specify main directory where to search protein folders');
    %wd = 'Z:\AntonioP_elltier1\CelllinesImaging\MitoSysPipelines';
    cd(wd);
end
if ~exist(fullfile(wd, 'ProcessingHelpFiles'));
    mkdir(fullfile(wd, 'ProcessingHelpFiles'));
end

if ~exist(fullfile(wd, 'ProcessingHelpFiles', 'protein2cfiles.mat')) || force
    [indirCell] = getAllFolders(wd, '(\Mito(s|S)ys\d)|(LSM880)');
    resfiles = [];
    for i = 1:length(indirCell)
        resfiles = [resfiles; getAllFiles(indirCell{i}, 'res', [session '.res'], 0)];
    end
    if isempty(resfiles)
        resftable = readtable(fullfile(wd, 'ProcessingHelpFiles', 'protein2cfiles.txt'),  'ReadVariableName', 0);
        for i=1:size(resftable,1)
            resfiles = [resfiles;resftable(i,:).Var1];
        end
        resfiles = cell(resfiles);
    end
    save(fullfile(wd, 'ProcessingHelpFiles', 'protein2cfiles.mat'), 'resfiles');
else
    load(fullfile(wd, 'ProcessingHelpFiles', 'protein2cfiles.mat'));
end
%%
%6 9 11
for ifile = 1:length(resfiles)
    resfile = resfiles{ifile};
    display(sprintf('master_workflow_fit_proteins processing res file %d/%d %s', ifile, length(resfiles), resfiles{ifile}))
    %fnames = workflow_fit_protein(resfile, session);
    fnames = {fullfile(fileparts(resfile), '1c_opt.res'), fullfile(fileparts(resfile),'2c_opt.res')};
    for iout = 1:length(fnames)
        distributefiles(fnames{iout})
    end
end
end

function status = appendFiles( readFile, writtenFile )
% APPENDFILES append readFile to writtenFile
fr = fopen( readFile, 'rt' );
fw = fopen( writtenFile, 'at' );
while feof( fr ) == 0
    tline = fgetl( fr );
    fwrite( fw, sprintf('%s\n',tline ) );
end
fclose(fr);
fclose(fw);
end

function distributefiles(resfile, patterns)
% DISTRIBUTEFILES read data in resfile and create resfiles for the
% different cellular classes
if nargin < 1
    [resfile, pathname] = uigetfile({'*.res'; '*.*'},'Select res file', 'C:\Users\toni\Dropbox\NPCMaturation\matlabcode\FCS\example_data\Protein');
    if ~resfile
        return
    end
    resfile = fullfile(pathname, resfile);
end

if nargin < 2
    patterns = {'\\\w+GFP\w+\\','\\\w+Cherry\w+\\' '\\POI\\', '\\POI_meta\\', '\\LSM\\', '\\WT\\'};
end
[path, basename] = fileparts(resfile);
res = readtable(resfile, 'Delimiter', '\t', 'Header', 1, 'FileType', 'text', 'TreatAsEmpty',{'NA'});
% get the header
filepre = fopen(resfile);
header = '';
for i = 1:2
    tline = fgetl(filepre);
    header = sprintf('%s%s\n',header,tline);
end
fclose(filepre);

%%
idxs = cell(1,length(patterns));
mdir = cell(1,length(patterns));
for i = 1:length(res.FullPath)
    for ip = 1:length(patterns)
        [idxst, idxend] = regexpi(res.FullPath{i},patterns{ip},  'start', 'end', 'matchcase');
        if ~isempty(idxst)
            if isempty(mdir{ip})
                mdir{ip}= res.FullPath{i}(1:idxend(1));
            end
            idxs{ip} = [idxs{ip};i];
        end
    end
end
%%

for ip = 1:length(patterns)
    if ~isempty(idxs{ip})
        tmpfile =  fullfile(mdir{ip}, [ 'tmp.res']);
        outfile  = fullfile(mdir{ip}, [ basename '.res']);
        writetable(res(idxs{ip},:), tmpfile ,  'WriteVariableNames',false, 'Delimiter', '\t','FileType', 'text');
        outfid = fopen(outfile, 'w');
        fprintf(outfid, header);
        fclose(outfid);
        appendFiles(tmpfile, outfile);
        delete(tmpfile);
    end
end
end

