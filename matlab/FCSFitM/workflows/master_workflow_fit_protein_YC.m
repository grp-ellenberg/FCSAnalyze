function master_workflow_fit_protein_YC(wd)
% MASTER_WORKFLOW_FIT_PROTEIN_YC call workflow_fit_protein for several directories
% specified in a file.
% This is a file to batch process the data from Yin Cai in batch
% Antonio Politi, EMBL, November 2017
folders = readtable('V:\MitoSys_data\Data\proteinfolders_YC_missed_files.txt', 'Delimiter','\t','ReadVariableNames',true);


session = '2c';
force = 1;
fid = fopen('V:\MitoSys_data\Data\process.log', 'w');
for ifile = 2:2%size(folders,1)
    try
        fprintf(fid, 'Process %d %s\n'  , ifile, folders.resfile{ifile});
        workflow_fit_protein(folders.resfile{ifile}, 'FAsession', session, ...
            'focVolfile', folders.volfile{ifile}, 'flintfile', folders.fintfile{ifile}, 'force', force, ...
            'weight', 0, 'wtfolder', 'WT');
        
    catch ME
        
        display(sprintf('Failed for %d %s',ifile,  folders.resfile{ifile}));
        getReport(ME, 'extended','hyperlinks','off')
        fprintf(fid, 'Failed for %d %s\n',ifile,  folders.resfile{ifile});
        fprintf(fid, '%s\n', getReport(ME, 'extended','hyperlinks','off'));
    end
end
fclose(fid);
end

function status = appendFiles( readFile, writtenFile )
% APPENDFILES append readFile to writtenFile
fr = fopen( readFile, 'rt' );
fw = fopen( writtenFile, 'at' );
while feof( fr ) == 0
    tline = fgetl( fr );
    fwrite( fw, sprintf('%s\n',tline ) );
end
fclose(fr);
fclose(fw);
end

function distributefiles(resfile, patterns)
% DISTRIBUTEFILES read data in resfile and create resfiles for the
% different cellular classes
if nargin < 1
    [resfile, pathname] = uigetfile({'*.res'; '*.*'},'Select res file', 'C:\Users\toni\Dropbox\NPCMaturation\matlabcode\FCS\example_data\Protein');
    if ~resfile
        return
    end
    resfile = fullfile(pathname, resfile);
end

if nargin < 2
    patterns = {'\\\w+GFP\w+\\','\\\w+Cherry\w+\\' '\\POI\\', '\\POI_meta\\', '\\LSM\\', '\\WT\\'};
end
[path, basename] = fileparts(resfile);
res = readtable(resfile, 'Delimiter', '\t', 'Header', 1, 'FileType', 'text', 'TreatAsEmpty',{'NA'});
% get the header
filepre = fopen(resfile);
header = '';
for i = 1:2
    tline = fgetl(filepre);
    header = sprintf('%s%s\n',header,tline);
end
fclose(filepre);

%%
idxs = cell(1,length(patterns));
mdir = cell(1,length(patterns));
for i = 1:length(res.FullPath)
    for ip = 1:length(patterns)
        [idxst, idxend] = regexpi(res.FullPath{i},patterns{ip},  'start', 'end', 'matchcase');
        if ~isempty(idxst)
            if isempty(mdir{ip})
                mdir{ip}= res.FullPath{i}(1:idxend(1));
            end
            idxs{ip} = [idxs{ip};i];
        end
    end
end
%%

for ip = 1:length(patterns)
    if ~isempty(idxs{ip})
        tmpfile =  fullfile(mdir{ip}, [ 'tmp.res']);
        outfile  = fullfile(mdir{ip}, [ basename '.res']);
        writetable(res(idxs{ip},:), tmpfile ,  'WriteVariableNames',false, 'Delimiter', '\t','FileType', 'text');
        outfid = fopen(outfile, 'w');
        fprintf(outfid, header);
        fclose(outfid);
        appendFiles(tmpfile, outfile);
        delete(tmpfile);
    end
end
end

