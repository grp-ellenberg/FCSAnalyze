function master_workflow_fit_dye_YC()
% MASTER_WORKFLOW_FIT_DYE call workflow_fit_dye for several directorie
% specified in the script
% 
% Antonio Politi, EMBL, January 2017

folders = readtable('V:\MitoSys_data\Data\alexafolders_YC_missed_values.txt', 'Delimiter',' ','ReadVariableNames',false)

% output subdiretoy to store results
outputdir = 'optimisedFit';




% range to fit kappa
kappaVal = [4:0.1:7]';
DCh1 = 464;
DCh2 = 521;
force = 0;
fid = fopen('V:\MitoSys_data\Data\process.log', 'w')
for idir = 1:length(folders.Var1)
    indir = folders.Var1{idir};
    display(['master_workflow_fit_dye processing directory ' num2str(idir) ' ' indir])
    xmlFAfiles = getAllFiles(indir, 'xml',['\.' '1c' '\.'], 0);
    zenfiles = getAllFiles(indir, 'fcs', [], 0);
    fprintf(fid, 'Process  %d  %s\n', num2str(idir), indir); 
    if isempty(xmlFAfiles)
        fprintf(fid, 'No 1c xml file found\n')
        display(['No 1c xml file found in  ' num2str(idir) ' ' indir])
        continue
    end
    try
         [focVolA, focVolfile, ChUsed] = workflow_fit_dye(indir, xmlFAfiles, zenfiles,  fullfile(indir, 'optimisedFit'), ...
            'kappaVal', [4:0.1:7]', 'force', force, 'diffCoeff', [DCh1, DCh2],'weight', 1, 'force' , 1);
    catch ME
        display(['failed master_workflow_fit_dye processing directory ' num2str(idir) ' ' indir ' ']);
        fprintf(fid, 'Failed for %d %s\n',ifile,  indir);
        fprintf(fid, '%s\n', getReport(ME, 'extended','hyperlinks','off'));
    end
end
fclose(fid)