#define MyAppName "FCSCalibration"
#define MyAppVersion "0.4.3"
#define MyAppExeName "FCSCalibration.bat"
#define MyAppPublisher "EMBL, Heidelberg"
#define MyAppURL "https://git.embl.de/grp-ellenberg/FCSAnalyze"
#define MyPath "P:\\Code\\FCSAnalyze\\R\\FCSCalibration"
#define MyDestPath "{%HOMEPATH}\\Programs\\FCSCalibration"
[Setup]
AppId = {{230d002a-a6f8-4ef3-9ee8-3e4b28d2e3ce}}
AppName = {#MyAppName}
DefaultDirName = {#MyDestPath}
DefaultGroupName = {#MyAppName}
OutputDir = {#MyPath}\dist
OutputBaseFilename = setup_{#MyAppName}
SetupIconFile = {#MyPath}\resources\FCSCalibration.ico
AppVersion = {#MyAppVersion}
AppPublisher = {#MyAppPublisher}
AppPublisherURL = {#MyAppURL}
AppSupportURL = {#MyAppURL}
AppUpdatesURL = {#MyAppURL}
PrivilegesRequired = lowest
Compression = lzma2/ultra64
SolidCompression = yes
VersionInfoVersion= 0.4.3
[Languages]
Name: "english"; MessagesFile: "compiler:Default.isl"

[Tasks]
Name: "desktopicon"; Description: "{cm:CreateDesktopIcon}"

[Files]
Source: {#MyPath}\*;DestDir: "{app}"; Excludes: ".git*,.Rproj*,*.Rhistory,tests\*"; Flags: ignoreversion recursesubdirs createallsubdirs;



[Icons]
Name: "{userdesktop}\{#MyAppName}"; Filename: "{app}\{#MyAppExeName}"; IconFilename: "{app}\resources\FCSCalibration.ico"

[Run]
Filename: "{app}\{#MyAppExeName}"; Description: "{cm:LaunchProgram,{#StringChange(MyAppName, '&', '&&')}}"; Flags: shellexec postinstall skipifsilent


