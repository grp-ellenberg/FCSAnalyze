# Packaging of the software using R-portable and innosetup


Installation through the compiled package is not recommended due to the large size of the installer. 

We follow the instructions from W. Lee Pang https://github.com/wleepang/DesktopDeployR


 

1. Download R-portable from e.g. 

https://sourceforge.net/projects/rportable/. Unpack it in `/dist`



2. Open one R-instance of R-portable and install the `fcsresfun` package using `/app/fcsresfun.R`, or directly

```

install.packages('devtools') # if not already installed

devtools::install('../../../fcsresfun', lib = '')

```



3. Run `FCSCalibration.bat`. The first time this may take a while as all dependencies need to be downloaded. Alternatively install the packages directly in Rportable. 
The list of packages is in the DESCRIPTION file of the fcsresfun package.



**To create a bundled package:** 



1. Install Innosetup  http://www.jrsoftware.org/



2. Load the script `./script/innosetup/fcscalibration.iss` into Innosetup. Change the path 

```

#define MyPath "path/to/package"

```

accordingly and run the installation. This will create a `dist/setup_FCSCalibration.exe` file.

