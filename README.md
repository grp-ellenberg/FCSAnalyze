# FCSAnalyze


> **Supplementary software 4** of
>
> Quantitative mapping of fluorescently tagged cellular proteins using FCS-calibrated four dimensional imaging
Antonio Z Politi, Yin Cai, Nike Walther, M. Julius Hossain, Birgit Koch, Malte Wachsmuth, Jan Ellenberg, 
[Nature Protocols 13, 1445–1464 (2018)](https://www.nature.com/articles/nprot.2018.040) 
> 
> Please cite the above work when using this tool.
>
> Published version is tag [v0.4.1_0.6](https://git.embl.de/grp-ellenberg/FCSAnalyze/tags/v0.4.1_0.6). We strongly recommend to use the most [recent tag](https://git.embl.de/grp-ellenberg/FCSAnalyze/tags)
> for your work as it contains bugfixes. 

Tools to process images and FCS data for FCS-calibrated imaging and high-throughput FCS. Data processing and detrending of the correlation curves uses Fluctuation Analyzer [FA](https://www-ellenberg.embl.de/resources/data_analysis) see also  [Wachsmuth et al. (2015)]( http://europepmc.org/abstract/MED/25774713) for details.

Please refer to the [WiKi](https://git.embl.de/grp-ellenberg/FCSAnalyze/wikis) for further details.

## Contents

## Directory fiji
Contains applications that must be run with a recent version of ImageJ (>1.51s) [FiJi](http://fiji.sc)

### fiji/FCSImageBrowser

Jython ImageJ plugin to quality control the images, compute the intensities at FCS measurement positions, and create a report used to compute the calibration curve.

### fiji/FCSCalibratedImage
Jython ImageJ plugin to convert fluorescence intensities to concentrations and protein number per pixel.

## Directory R
Applications for R. We suggest to use [RStudio](https://www.rstudio.com/) for usage of the tool. A compiled version can be downloaded from https://www-ellenberg.embl.de/resources/data-analysis.
### R/FCSCalibration
A shiny R App to compute the calibration curve and inspect the calibration data.

## Directory matlab
Applications for [MATLAB](http://www.mathworks.com)
## matlab/FCSFitM
A MATLAB tool to compute fits to the auto-correlation functions, cross-correlation function, and compute the effective volume given fluorescent dye data. The tool uses fluorescene intensities computed from FCSImageBrowser.

## Directory example\_data
Contains example_data to test the programs. 
### example\_data\FCSFitM
Contains correlation data as obtained for a fluroescent dye and fluorescent protein to be fitted FCSFitM. See README in the corresponding directory.

Before performing a fit of the correlation for the fluorescent protein FCSFitM requests to compute the fluorescene intensities at the FCS measurment point. For this use FCSImageBrowser.

### example\_data\FCSCalibration
Contains concentrations and fluorescence intensity data as derived from FCSFitM. The data is read in FCSCalibration to compute an FCS calibration coefficient for FCS calibrated imaging.

## Versioning
Versioning is combined version of the FCSCalibration and FCSFitM tools `vFCSCalibration_FCSFitM`





# Author:
Antonio Politi, EMBL, 2017-2018

mail@apoliti.de

