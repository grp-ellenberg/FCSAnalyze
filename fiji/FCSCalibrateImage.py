'''
fcsCalibrateImage
Tools with GUI to perform a linear transformation of fluorescence intensity in a channel using parameters obtained from a FCS calibration pipeline.
    inputs:
        calibration file: A tab delimited text file that contains calibration parameters as generated during the FCS calibration pipeline.
            name    baseline    slope
            C_nM    193.165     0.129232490086972
        image file: An image file that contains metadata for the image dimensions and pixelsize. Conversion to protien number is only performed for 3D and 4D images.
    outputs:
        calibrated images: Create two images 32-bits images for concentrations in nM and protein numbers per pixel with suffix '_C' and '_N'. 
        Optionally save the images to tif files in the directory of the original image.
        
EMBL, June, 2017
Antonio Politi
'''

# Imports
import ij
import re
import os
from ij.io import OpenDialog, DirectoryChooser
from ij.gui import PointRoi, Roi, YesNoCancelDialog, MessageDialog, Overlay
from ij.plugin.frame import RoiManager
from ij.process import ImageConverter
from ij.plugin import MontageMaker
from loci.plugins import BF
from loci.plugins.in import ImporterOptions

from glob import glob
from ij import IJ, ImagePlus, ImageStack
from xml.etree import ElementTree as ET
from xml.etree.ElementTree import Element, SubElement, Comment, tostring
from xml.dom import minidom
from javax.swing import (BoxLayout, JScrollPane, ImageIcon, JButton, JRadioButton, ButtonGroup, JFrame, JPanel,
 JSeparator, JPasswordField, JLabel, JTextArea, JTextField, JScrollPane, JCheckBox, JList, DefaultListModel, JScrollPane, 
        SwingConstants, WindowConstants, SpringLayout, Box, JComboBox, JSeparator, BorderFactory, JList, JFileChooser)
from java.awt import Component, GridLayout, BorderLayout, Dimension, GridBagLayout, GridBagConstraints
from ij.plugin import ChannelSplitter
import java.awt.event.ActionEvent
import java.awt.event.ActionListener
import thread
from java.awt import Color, Dimension
import csv
from java.io import File 


class FcsCalibrateImage(java.awt.event.ActionListener, object):
    chooser = JFileChooser()
    lookUpTable = ["Cyan Hot", "Fire", "Green", "Red", "Magenta Hot"] # look up table for Intensity, Concentration and number of particles
    displayRanges = [[0, 2000], [0, 60]] # default range for concentrations and number of molecules

    comboLookUpTable = list()   #Contains GUI elements for lookup up table  
    comboFCSChannel = list()    #Contains GUI elements for Imagechannel to FCS mapping
    minC = list()               #Minmal value
    maxC = list()               #Maximal value
    lastdir = ''
    version = '0.2'
    def __init__(self, resfile = ""):
        '''
        Creates the gui
        '''
        
        ''' 
        Main frame
        '''
        
        self.frameMain = JFrame('FCSCalibrateImage v.' + self.version,
            defaultCloseOperation = JFrame.DISPOSE_ON_CLOSE,
            size = (900, 400)
        )
        pane = self.frameMain.getContentPane()
        
        '''
        Control buttons for switching images
        '''
        panel = JPanel()
        panel.setLayout(BoxLayout(panel, BoxLayout.LINE_AXIS))
        panel.add(JButton('Load calibration', actionPerformed=self.loadcalibration))
        self.calfile = JTextField("", 15)
       
        panel.add(self.calfile)
        self.slope = JTextField("0", 2)
        self.basel = JTextField("0", 2)
        panel.add(JLabel('slope'))
        panel.add(self.slope)
        panel.add(JLabel('basel'))
        panel.add(self.basel)
        pane.add(panel, BorderLayout.PAGE_START)
 
        # list of names
        panel = JPanel()
        panel.setLayout(GridBagLayout())
        c = GridBagConstraints()
       
        
        colors = []
        
        for i in range(0, 2):
            self.comboLookUpTable.append(JComboBox(self.lookUpTable))
            self.minC.append(JTextField(str(self.displayRanges[i][0]),5))
            self.maxC.append(JTextField(str(self.displayRanges[i][1]),5))
            self.comboLookUpTable[i].setSelectedIndex(i)
        
        c.weighty = 0.1
        c.weightx = 0.1
        c.fill = GridBagConstraints.HORIZONTAL
        c.gridx = 0
        c.gridy = 0
        panel.add(JLabel(''), c)
        c.weightx = 0.5
        c.gridx = 1
        c.gridy = 0
        panel.add(JLabel('Look up table'), c)
        c.gridx = 2
        c.gridy = 0
        panel.add(JLabel('Min'), c)
        c.gridx = 3
        c.gridy = 0
        panel.add(JLabel('Max'), c)

        
        labelsGraph = ['Conc [nM]', 'N']
        for i in range(0,2):
            c.weightx = 0.1
            c.gridy = i+1
            c.gridx = 0
            panel.add(JLabel(labelsGraph[i]),c)
            c.weightx = 0.5
            c.gridx = 1
            panel.add(self.comboLookUpTable[i],c)
            c.gridx = 2
            panel.add(self.minC[i],c)
            c.gridx = 3
            panel.add(self.maxC[i],c)

        self.filedata = DefaultListModel()
        self.filelist = JList(self.filedata)
        listScroller = JScrollPane(self.filelist);
        listScroller.setPreferredSize(Dimension(250, 120));
        c.ipady = 40;  
        c.weightx = 0.5
        c.gridx = 0
        c.gridy = 4
        c.gridwidth = 4
        panel.add(listScroller,c)
        
        pane.add(panel,  BorderLayout.CENTER)
        
        # action buttons
        panel = JPanel()
        self.save = JCheckBox("Save image", 0)
        self.show = JCheckBox("Show", 1)
        self.savecb = JCheckBox("Save color bar", 0)
        self.negative_to_zero = JCheckBox("Negative pixels to 0", 0)
        panel.setLayout(BoxLayout(panel, BoxLayout.LINE_AXIS))
        panel.add(JButton('Add file(s)', actionPerformed=self.addfile))
        panel.add(JButton('Clear file(s)', actionPerformed=self.clearfile))
        panel.add(JLabel('Channel to convert'))
        self.channelconv = JComboBox(range(1,10))
        panel.add(self.channelconv)
        panel.add(self.save)
        panel.add(self.negative_to_zero)
        panel.add(self.savecb)
        panel.add(self.show)
        panel.add(JButton('Process file(s)', actionPerformed=self.processfile))
        pane.add(panel, BorderLayout.SOUTH)
        pane.setBorder(BorderFactory.createEmptyBorder(20, 20 ,20,20))
        self.frameMain.visible = 1


    def loadcalibration(self, event):
        '''
        load file containing calibration data
        '''
        
        od = OpenDialog("Choose calibration file", None, 'calibration.txt')
        if od.getFileName() == None:
            return None
        self.lastdir = od.getDirectory()
        fname = os.path.join(od.getDirectory(), od.getFileName())
        filein = open(fname, 'rb')
        cal = csv.reader(filein, delimiter = '\t')
        cal = list(cal)
        try:
            if cal[1][0] == 'C_nM':
                self.slope.setText(cal[1][2])
                self.slope.setCaretPosition(0)
                self.basel.setText(cal[1][1])
                self.basel.setCaretPosition(0)
                self.calfile.setText(fname)
                self.calfile.setCaretPosition(0)
            else:
                IJ.showMessage('File %s does not contains parameters for FCS calibration. Enter the parameters manually' % fname)
        except:
            IJ.showMessage('File %s does not contains parameters for FCS calibration. Enter the parameters manually' % fname)
        filein.close()
        return


    def addfile(self, event):
        '''
        addfile to list in GUI
        '''
        # TODO add a filefilter
        try:
            self.chooser.setMultiSelectionEnabled(1)
            self.chooser.setCurrentDirectory(File(self.lastdir))
            keypressed = self.chooser.showOpenDialog(self.frameMain)
            if keypressed != JFileChooser.APPROVE_OPTION:
                return
            files = self.chooser.getSelectedFiles()
            
            if len(files) > 0:
                for filen in files:
                    self.lastdir = os.path.basename(str(filen))
                    self.filedata.addElement(filen)

        except Exception as inst:
            print('addfile')
            print(inst)

    def clearfile(self, event):
        self.filedata.clear()

    def processfile(self, event):
        '''
        convert one or several files
        '''
        try:
            idxs = self.filelist.getSelectedIndices()
            if len(idxs) == 0:
                return
            print(idxs)
            for idx in idxs:
                fname  = str(self.filedata.get(idx))
                print idx
                print(os.path.isfile(fname))
                if not os.path.isfile(fname):
                    IJ.showMessage( 'File %s does not exist' % fname)
                    return
                slope = float(self.slope.getText())
                basel = float(self.basel.getText())
                if slope == 0:
                    IJ.showMessage( 'No calibration  slope has been defined')
                    return
                self.fcscalibrateimage(fname, int(self.channelconv.getSelectedItem())-1, {'slope': slope, 'basel': basel})
        except Exception as inst:
                print('processfile')
                print(inst)


    def fcscalibrateimage(self, fname, channel, linefit):
        '''
        Apply linear transformation to channel of image located at fname. Use parameters of lineFit
            input:
                fname   - filename of image to open
                channel - channel to convert
                linefit - [slope, baselin] of FCS calibration
        '''

        IJ.run("Close All", "")
        bfopt = ImporterOptions()
        bfopt.setId(fname)
        try:
            img = BF.openImagePlus(bfopt)[0]
        except:
            IJ.showMessage(sprintf('Could not open %s', fname))
            return
        
        # pixel volume in um3
        V = img.getCalibration().pixelWidth*img.getCalibration().pixelHeight*img.getCalibration().pixelDepth;
        Na = 0.602214086
        if round(img.getCalibration().pixelDepth,3)  == 0:
            IJ.showMessage('For file %s\nConvert image to molecule number requires a 3D stack. Only compute concentrations' %fname)
            idxs = [0]
        else:
            idxs = [0, 1]

        coeff = V*Na;
        #compute concentration for each voxel
        formulaC = "%f*(v-%f)" %(linefit["slope"], linefit["basel"])
         #compute number of particles for each volume
        formulaN = "%f*%f*(v-%f)" %(coeff, linefit["slope"], linefit["basel"])
        formulas = [formulaC, formulaN]

        chSp = ChannelSplitter()
        imageC = chSp.split(img)
        calImg = [ imageC[channel].duplicate(), imageC[channel].duplicate()]
        endName = ["_C", "_N"]
        for idx in idxs:
            # convert to 32 bit to have doubles
            IJ.run(calImg[idx], "32-bit", "")
            # convert to concentration or number of molecules 
            IJ.run(calImg[idx], "Macro...", "code=v="+ formulas[idx] +" stack");
            
            if self.negative_to_zero.isSelected():
            	# remove values below 0
            	IJ.run(calImg[idx], "Macro...", "code=v=v*(v>=0) stack");
       		# for global quantification one should use the values also with negative values
       		
        #calibrationBar size
        imgCal = [110, 300]
        scaleB = IJ.createImage("Calibration number of particles", "32-bit", imgCal[0], imgCal[1], 1)
        scaleB = [scaleB.duplicate(), scaleB.duplicate()]
        
        if self.show.isSelected():
            img.show()
        
        for i in idxs:
            calImg[i].setTitle(os.path.splitext(os.path.split(fname)[1])[0] + endName[i])
            IJ.run(calImg[i], self.lookUpTable[i], "")
            
            calImg[i].setDisplayRange(float(self.minC[i].getText()), float(self.maxC[i].getText()), 0)
            scaleB[i].setDisplayRange(float(self.minC[i].getText()), float(self.maxC[i].getText()), 0)
            scaleB[i].setTitle(os.path.splitext(os.path.split(fname)[1])[0] + endName[i] + "_CalBar")
            IJ.run(scaleB[i], self.lookUpTable[i], "")
            scaleB[i].setRoi(0,0,1,1)
            IJ.run(scaleB[i], "Calibration Bar...", "location=[At Selection] fill=Black label=White number=5 decimal=0 font=10 zoom=2 bold overlay")
            scaleB[i].deleteRoi()
            
            if self.show.isSelected():
                scaleB[i].show()
                calImg[i].show()
            
            scaleB[i] = scaleB[i].flatten()
            
            if  self.save.isSelected():
                IJ.saveAsTiff(calImg[i], os.path.splitext(fname)[0] + endName[i])
            if self.savecb.isSelected():
                IJ.saveAsTiff(scaleB[i], os.path.splitext(fname)[0] + endName[i] + "_CalBar")


cls = FcsCalibrateImage()